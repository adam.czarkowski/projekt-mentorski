from rest_framework import serializers
from .models import *


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = '__all__'


class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = '__all__'


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = '__all__'


class ProfessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profession
        fields = '__all__'


class QualificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Qualification
        fields = '__all__'


class SubqualificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subqualification
        fields = '__all__'


class LearningEffectSerializer(serializers.ModelSerializer):
    class Meta:
        model = LearningEffect
        fields = '__all__'


class VerificationCriterionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VerificationCriterion
        fields = '__all__'


class AsignmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Asignment
        fields = '__all__'


