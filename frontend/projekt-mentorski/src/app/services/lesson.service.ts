import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable } from 'rxjs';
import { Lesson } from '../models/lesson';

const baseUrl = 'http://localhost:8000/api/lessons/?format=json';

@Injectable({
  providedIn: 'root'
})
export class LessonService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Lesson[]> {
    return this.http.get<Lesson[]>(baseUrl);
  }
}
