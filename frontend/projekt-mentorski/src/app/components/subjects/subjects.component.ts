import { LessonService } from '../../services/lesson.service';
import { Lesson } from '../../models/lesson';
import { SubjectService } from '../../services/subject.service';
import { Subject } from '../../models/subject';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {

  /**
   * s - subjects
   * l - lessons
   */

  s: Subject[];
  l: Lesson[];
  selectedS: Subject;
  selectedL: Lesson;
  assignedL: Lesson[];
  
  constructor(
    private SubjectService: SubjectService,
    private LessonService: LessonService
    ){
  }
  
  ngOnInit() {
    this.getL();
    this.getS();
  }

  getL(): void {
    this.LessonService.getAll().subscribe(
      (lessons: Lesson[]) => {this.l = lessons})
  }

  getS(): void {
    this.SubjectService.getAll().subscribe(
      (subjects: Subject[]) => {
        this.s = subjects;
        this.onSelectS(this.s[1])
      }
    );  
  }

  onSelectS(subject: Subject): void {
    this.selectedS = subject;
    this.getLOf(subject.id)
  }

  onSelectL(lesson: Lesson): void {
    this.selectedL = lesson;
  }

  getLOf(subjectId: number): void {
    this.assignedL = [];
    for (let i=0; i < this.l.length; i++) {
      if (this.l[i].subject === subjectId) {
        this.assignedL.push(this.l[i])
      }
    }
  }
}
