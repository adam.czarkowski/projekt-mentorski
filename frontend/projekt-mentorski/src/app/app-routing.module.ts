import { MainComponent } from './components/main/main.component';
import { QualificationsComponent } from './components/qualifications/qualifications.component';
import { SubjectsComponent } from './components/subjects/subjects.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'subjects', component: SubjectsComponent },
  { path: 'qualifications', component: QualificationsComponent },
  { path: 'main', component: MainComponent },
  { path: '', redirectTo: '/main', pathMatch: 'full' }
];



@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
