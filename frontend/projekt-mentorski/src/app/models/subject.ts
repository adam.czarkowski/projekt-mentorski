export interface Subject {
    id: number;
    name: string;
    year: number;
}