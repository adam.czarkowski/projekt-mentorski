export interface Lesson {
    id: number;
    lessonNumber: number;
    topic: string;
    subject: number;
    section: number;
}